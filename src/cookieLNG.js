import { useState, useEffect } from "react";
import { useCookies } from 'react-cookie'

const cookieLNG = () => {
    const [CookieData, setCookieData] = useState()
    useEffect(() => {
        setCookieData(useCookies(['lng']))
    }, []);
    return CookieData.lng
  }

export default cookieLNG;