import React from "react"
import Button from "components/CustomButtons/Button.js";
import Media from 'react-media';
import classNames from "classnames";

import { withTranslation } from 'react-i18next';

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/views/componentsSections/completedStyle.js";

import ShowTag from "./Sections/tags.js";

import { isPropertySignature } from "typescript";

import ButtonsRegistries5 from "./Sections/ButtonsRegistries5.js";

import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

const useStyles = makeStyles(styles);

function ButtonsEngine(props) {
    let item = props.item
    const classes = useStyles();
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
    }  

    let value

    function handleChange(event) {   
         value = event.target.value; 
         }

    function handleSubmit(event) {
        alert('Um nome foi enviado: ' + value);
        event.preventDefault();
    }

    shuffle(item);
    const ShowRegistryComponent = item.map(item => <ButtonsRegistries5 key={item.id} language={item.language} id={item.id} ENtitle={item.ENtitle} BRtitle={item.BRtitle} tr={item.type_of_registry} img={item.images[0].image} />);
    
    return (
        
            <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"transparent",paddingTop:"100px", paddingLeft:"90px",paddingRight:"90px",borderRadius:"38px",borderTop:"2px black darkgrey",top:'50%'}}>
            <div style={{textAlign:'center'}}>
                <form onSubmit={handleSubmit} style={{paddingBottom:'60px'}}>
                <input type="text" placeholder="Search.." onChange={handleChange} value={value} style={{background:'rgba(0,0,0,0.3)',width:'600px',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',color:"lightgrey",fontFamily: "Courier New",paddingLeft:'10px'}} />
                <select id="types" name="types" style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
                <option value="" style={{display: "none"}} selected="selected">Types</option>
                <option value="volvo">Close Encounters</option>
                <option value="saab">History/Archeology</option>
                <option value="fiat">Miscellaneous</option>
                <option value="audi">Documentary</option>
                </select>
                <select id="tags" name="tags" style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
                <option value="" style={{display: "none"}} selected="selected">Tags</option>
                <option value="volvo">Volvo</option>
                <option value="saab">Saab</option>
                <option value="fiat">Fiat</option>
                <option value="audi">Audi</option>
                </select>
                <select id="tags" name="tags" style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
                <option value="" style={{display: "none"}} selected="selected">Lng</option>
                <option value="volvo">Eng</option>
                <option value="saab">Pt-Br</option>
                
                </select>
                <input type="submit" value="   Send   " style={{background:'rgba(0,0,0,0.7)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',color:"rgba(100,100,100,1)",fontFamily: "Courier New",cursor:'pointer'}}></input> 
                </form>
            </div>
        
            <GridContainer>
                {ShowRegistryComponent}
            </GridContainer>
            
         </div>
         
    )
    
}


export default ButtonsEngine;         