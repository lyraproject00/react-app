import React, { useState, useEffect } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import './theme-Custom.css';

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// @material-ui/icons
import background from 'assets/img/prism6.svg'
import axios from "axios";
import { useParams } from "react-router-dom";
import Spinner from '@atlaskit/spinner';
import Media from 'react-media';
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import MapButton from "../Pages/Sections/MapButton.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import Showlinks from "./Sections/showlinks.js";
import ShowVideos from "./Sections/showvideos.js";
import EditedCarousel from "../Components/Sections/EditedCarousel.js"
import Regis from "../Components/Sections/regis.js"

//import issloading from "./Sections/islloading.js"

//import ReactPlayer from 'react-player/lazy'
import styles from "assets/jss/material-kit-react/views/landingPage.js";

// Sections for this page

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  
  const { id } = useParams();
  const [isLoading, setLoading] = useState(true);
  const [items, setItem] = useState();

  //"https://lyraproject0.herokuapp.com/api/registry/"
  //"http://localhost:8000/api/registry/"

  useEffect(() => {
    axios.get("https://lyraproject0.herokuapp.com/api/registry/"+id).then(response => {
      setItem(response.data);
      setLoading(false);
    });
  }, []);

  if (isLoading) {
    let lng
    
  const resources= {
      en: {
        translation: {
         header1: "About",
         header2: "Database" ,
         header3: "Map",
         header4: "Pages"
        }
      },
      pt_br: {
        translation: {
          header1: "Sobre",
          header2: "Banco de dados",
          header3: "Mapa",
          header4: "Páginas"
        }
      }
    }

  switch (document.cookie) {
  case 'lng=en':
      lng = 'en'
      break;
  case 'lng=pt_br':
      lng = 'pt_br'
      break;
  }
  
  i18n
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
      resources,
      //detection: options,
      lng: lng ? lng : 'en',
      debug: true,
      fallbackLng: 'en',
      supportedLngs: ['en', 'pt_br'],
      interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
      },

  
});
    return(
      <div style={{backgroundColor:"rgb(1, 1, 1)"}}>
          <Header
              color="transparent"
              routes={dashboardRoutes}
              brand="Lyra Project"
              rightLinks={<HeaderLinks />}
              fixed
              changeColorOnScroll={{
              height: 500,
              color: "dark"
              }}
              {...rest}
          />
          <Parallax filter style={{height:"1000px",borderBottom:'0px solid black',borderRadius:'0px'}} image={require("assets/img/landscapes/12389-plain-mountains-landscape-twilight-stars-deserted-4k.jpg")}>
              <div className={classes.container}>
              <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                  </GridItem>
              </GridContainer>
              </div>
          </Parallax>
          <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"transparent",paddingLeft:"0px",paddingRight:"0px",marginBottom:'100px'}}>
              
                  
                  <div style ={{textAlign: "center"}}>
                  <Spinner size="xlarge"/> <h1 style ={{display: "inline-block",paddingTop:"30px"}}>Loading Database...</h1><p></p>
                  </div>
                  
              
          </div>
          <Footer />
          </div>
  );
  }

  let lng
    
  const resources= {
      en: {
        translation: {
        titlelinks: 'External links:',
         title: items.ENtitle,
         text: items.ENtext,
         mapa : "Open on Map",
         header1: "About",
         header2: "Database" ,
         header3: "Map",
         header4: "Pages"
        }
      },
      pt_br: {
        translation: {
          titlelinks: 'Links externos:',
          title: items.BRtitle,
          text: items.BRtext,
          mapa : "Abrir no Mapa",
          header1: "Sobre",
          header2: "Banco de dados",
          header3: "Mapa",
          header4: "Páginas"
        }
      }
    }

  switch (document.cookie) {
  case 'lng=en':
      lng = 'en'
      break;
  case 'lng=pt_br':
      lng = 'pt_br'
      break;
  }
  
  i18n
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
      resources,
      //detection: options,
      lng: lng ? lng : 'en',
      debug: true,
      fallbackLng: 'en',
      supportedLngs: ['en', 'pt_br'],
      interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
      },

  
});

  const image = require("assets/img/gicon2.png")
  //const ShowImagesComponent = item.map(item => <Showimages key={item.id} img={item.images} />);
  return (
    <Media queries={{
      small: "(max-width: 599px)",
      medium: "(min-width: 600px) and (max-width: 1199px)",
      large: "(min-width: 1200px)"
    }}>
        {matches => (
    <div style={{backgroundImage: `url(${background})`}}>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Lyradb"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 550,
          color: "dark"
        }}
        {...rest}
      />
      <Parallax style={{height:"980px"}} filter image={require("assets/img/landscapes/12389-plain-mountains-landscape-twilight-stars-deserted-4k.jpg")}>
        
      </Parallax>

      <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"rgba(20,20,20,0.4)",paddingLeft:"30px",paddingRight:"30px",paddingTop:"0px",margin:"4px",border: "4.5px solid black"}}>
        <div className={classes.container} style={{maxWidth: "1395px"}}>
          <Regis item={items} />
          
          <EditedCarousel img={items.images}/> 
              
          <MapButton items={items}/>
          <br/> <br/>
          <ShowVideos/>

          {matches.small && <Showlinks/>}
          {matches.medium && <Showlinks/>} 
          {matches.large && <Showlinks/>}

        </div>
      </div>
    <Footer />
    </div>
    )}</Media>
  );
}