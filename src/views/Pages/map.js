import React, { useState, useEffect } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import classNames from "classnames";

import 'leaflet/dist/leaflet.css'

import { MapContainer, TileLayer, Popup, Marker } from 'react-leaflet'

//import Marker from 'react-leaflet-enhanced-marker'
// @material-ui/icons
import axios from "axios";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";

import styles from "assets/jss/material-kit-react/views/landingPage.js";

import L from 'leaflet';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

const position = [51.505, -0.09]

export default function Map(props) {

    const classes = useStyles();
    const { ...rest } = props;
    
    const [isLoading, setLoading] = useState(true);
    const [items, setItems] = useState();

    useEffect(() => {
        axios.get("http://localhost:8000/api/registry/").then(response => {
          setItems(response.data);
          setLoading(false);
        });
      }, []);
    
    if (isLoading) {
    return(
        <div>aaaaaaaaaaaaaa</div>
    );

    }    
    const markers = items.map(item => <Marker position={[item.localizations[0].lat, item.localizations[0].lng]}>
                                        <Popup>  A pretty CSS3 popup. <br /> Easily customizable. </Popup>
                                        </Marker> );
    return(
      
        <div style={{backgroundColor:"rgb(1, 1, 1)"}}>
        <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Lyra Project"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
            height: 550,
            color: "dark"
            }}
            {...rest}
        />
        <Parallax filter image={require("assets/img/bg7.jpg")}>
                
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"black",paddingLeft:"0px",paddingRight:"0px"}}>
        <MapContainer center={position} zoom={5} scrollWheelZoom={true} style={{ height: 900, width: 1630 }} >
        <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {markers}
    </MapContainer>
    </div>
    </div>
    );
    
}