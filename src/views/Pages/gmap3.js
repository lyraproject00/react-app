import React, { Fragment, Component } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import { Map, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';

import classNames from "classnames";

//import Marker from 'react-leaflet-enhanced-marker'
// @material-ui/icons
import axios from "axios";

// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import styles from "assets/jss/material-kit-react/views/landingPage.js";

const dashboardRoutes = [];


    const useStyles = makeStyles(styles);


const mapStyles = {
  width: '100%',
  height: '100%',
};

export class MapContainer extends Component {
    state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      registries_list : []
    };
   
    componentDidMount() {
        this.refreshRList();
      }

    refreshRList = () => {
    axios
        .get("http://localhost:8000/api/registry/")
        .then(res => this.setState({ registries_list: res.data }))
        .catch(err => console.log(err));
    };

    onMarkerClick = (props, marker, e) =>
      this.setState({
        selectedPlace: props,
        activeMarker: marker,
        showingInfoWindow: true
      });
   
    onMapClicked = (props) => {
      if (this.state.showingInfoWindow) {
        this.setState({
          showingInfoWindow: false,
          activeMarker: null
        })
      }
    };
    
    

    render() {
            
      return (
        
        <div style={{backgroundColor:"rgb(1, 1, 1)"}}>
        <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Lyra Project"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
            height: 550,
            color: "dark"
            }}
    
        />
        <Parallax style={{height:"1080px"}} filter image={require("assets/img/bg7.jpg")}>
                
        </Parallax>
        
        <div  style={{backgroundColor:"black",paddingLeft:"0px",paddingRight:"0px"}}>
        <Map google={this.props.google}
            zoom={5}
            mapType="hybrid"
            initialCenter={{ lat: 38.8757676, lng: -99.0257517}}
            onClick={this.onMapClicked}>
       <Marker 
            onClick={this.onMarkerClick}
            position={{
                lat: 38.8757676,
                lng: -99.0257517
              }}
              name={"aaa"
              }
              
        />

        <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}>
              <div>
              <a href={"http://localhost:3000/registry/"}>link text</a>
              </div>
          </InfoWindow>
    
          </Map>
        </div>
        </div>
      )
    }
  }


export default GoogleApiWrapper({
  apiKey: 'AIzaSyA8k2utFV196NDfL2ioPEkta0DGgMF9i0I'
})(MapContainer);
