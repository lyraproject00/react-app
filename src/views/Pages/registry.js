import React, { useState, useEffect } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

import axios from "axios";
import { useParams } from "react-router-dom";
import Spinner from '@atlaskit/spinner';

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import Showlinks from "./Sections/showlink.js";
import EditedCarousel from "../Components/Sections/EditedCarousel.js"
import Regis from "../Components/Sections/regis.js"
import Regis2 from "./Sections/regis2.js"

//import issloading from "./Sections/islloading.js"

import ReactPlayer from 'react-player'
//import ReactPlayer from 'react-player/lazy'
import styles from "assets/jss/material-kit-react/views/landingPage.js";

// Sections for this page

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  
  const { id } = useParams();
  const [isLoading, setLoading] = useState(true);
  const [item, setItem] = useState();

  //"https://lyraproject0.herokuapp.com/api/registry/"
  //"http://localhost:8000/api/registry/"

  useEffect(() => {
    axios.get("http://localhost:8000/api/registry/"+id).then(response => {
      setItem(response.data);
      setLoading(false);
    });
  }, []);

  if (isLoading) {
    return(
      <div style={{backgroundColor:"rgb(1, 1, 1)"}}>
          <Header
              color="transparent"
              routes={dashboardRoutes}
              brand="Lyra Project"
              rightLinks={<HeaderLinks />}
              fixed
              changeColorOnScroll={{
              height: 500,
              color: "dark"
              }}
              {...rest}
          />
          <Parallax filter image={require("assets/img/landing-bg.jpg")}>
              <div className={classes.container}>
              <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                  </GridItem>
              </GridContainer>
              </div>
          </Parallax>
          <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"black",paddingLeft:"0px",paddingRight:"0px"}}>
              
                  
                  <div style ={{textAlign: "center"}}>
                  <Spinner size="xlarge"/> <h1 style ={{display: "inline-block",paddingTop:"30px"}}>Loading Database...</h1><p></p>
                  </div>
                  
              
          </div>
          <Footer />
          </div>
  );
  }
  //const ShowImagesComponent = item.map(item => <Showimages key={item.id} img={item.images} />);
  return (
    <div style={{backgroundColor:"grey"}}>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Lyradb"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 550,
          color: "dark"
        }}
        {...rest}
      />
      <Parallax filter image={require("assets/img/landing-bg.jpg")}>
        
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"rgb(6, 6, 6)",paddingLeft:"30px",paddingRight:"30px",paddingTop:"50px",margin:"4px",border: "6px solid black"}}>
        <div className={classes.container} style={{maxWidth: "1550px"}}>
          <Regis title={item.title} text={item.description} />

          <EditedCarousel img={item.images}/>  

          <div style={{backgroundColor: "rgb(3, 3, 3)",borderRadius:"50px",paddingLeft:"7.5%",borderLeft: "0px inset black",border: "6px solid black",marginBottom:"40px",paddingTop:"4%"}}>
          <div style={{width:"42%",display:"inline-block",marginBottom:"90px",marginRight:"8%",marginTop:""}}>
          <ReactPlayer width="100%" height="300px" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
          <div style={{marginTop:"25px",marginLeft:"5%",paddingLeft:"9px",width:"max-content",paddingRight:"9px",background:"rgb(14, 14, 14)",borderTopRightRadius:"10px",borderBottomLeftRadius:"10px",borderLeft:"1px solid darkgrey",fontStyle: "oblique",fontSize:"17px",fontFamily:"Courier New",borderRight:"1px solid darkgrey",alignSelf:"center"}}>Video sobre abduzido canal et</div>
          </div>
          <div style={{width:"42%",display:"inline-block",marginBottom:"90px",marginRight:"8%",marginTop:""}}>
          <ReactPlayer width="100%" height="300px" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
          <div style={{marginTop:"25px",marginLeft:"5%",paddingLeft:"9px",width:"max-content",paddingRight:"9px",background:"rgb(14, 14, 14)",borderTopRightRadius:"10px",borderBottomLeftRadius:"10px",borderLeft:"1px solid darkgrey",fontStyle: "oblique",fontSize:"17px",fontFamily:"Courier New",borderRight:"1px solid darkgrey"}}>Video sobre abduzido canal et</div>
          </div>
          <div style={{width:"42%",display:"inline-block",marginBottom:"90px",marginRight:"8%",marginTop:""}}>
          <ReactPlayer width="100%" height="300px" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
          </div>
          </div>

            <div>
              <Showlinks link={item.links}/>
            </div>
      </div>
    </div>
    <Footer />
    </div>
  );
}