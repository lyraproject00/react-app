import React, { useState, useEffect } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

import background from 'assets/img/alie.jpg'

import axios from "axios";
import Spinner from '@atlaskit/spinner';

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import Media from 'react-media';


import styles from "assets/jss/material-kit-react/views/landingPage.js";

import ButtonsRegistries from "./Sections/ButtonsRegistries.js";

import ButtonsRegistries2 from "./Sections/ButtonsRegistries2.js";

import ButtonsRegistries3 from "./Sections/ButtonsRegistries3.js";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  
  const [isLoading, setLoading] = useState(true);
  const [items, setItems] = useState();

  //"http://localhost:8000/api/registry/"
  //"https://lyraproject0.herokuapp.com/api/registry/"
  useEffect(() => {
    axios.get("http://localhost:8000/api/registry/").then(response => {
      setItems(response.data);
      setLoading(false);
    });
  }, []);

  

  function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }  

  if (isLoading) {
    return(
      <div style={{backgroundColor:"rgb(1, 1, 1)"}}>
          <Header
            color="transparent"
            routes={dashboardRoutes}
            brand="Lyra Project"
            rightLinks={<HeaderLinks />}
            fixed
            changeColorOnScroll={{
              height: 550,
              color: "dark"
              }}
              {...rest}
          />
          <Parallax filter image={require("assets/img/2.jpg")}>
              <div className={classes.container}>
              <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                  </GridItem>
              </GridContainer>
              </div>
          </Parallax>
          <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"black",paddingLeft:"0px",paddingRight:"0px"}}>
              
                  
                  <div style ={{textAlign: "center"}}>
                  <Spinner size="xlarge"/> <h1 style ={{display: "inline-block",paddingTop:"30px"}}>Loading Database...</h1><p></p>
                  </div>
                  
              
          </div>
          <Footer />
          </div>
  );
  }
  shuffle(items);
  const ShowRegistryComponent = items.map(item => <ButtonsRegistries3 key={item.id} language={item.language} id={item.id} ENtitle={item.ENtitle} BRtitle={item.BRtitle} tr={item.type_of_registry} img={item.images[0].image} />);
  return (
    <Media queries={{
      small: "(max-width: 599px)",
      medium: "(min-width: 600px) and (max-width: 1299px)",
      large: "(min-width: 1300px)"
    }}>
        {matches => (
    <div style={{backgroundImage: `url(${background})`,backgroundSize: 'contain'}}>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Lyra Project"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 550,
          color: "dark"
        }}
        {...rest}
      />
      <Parallax filter style={{height:"1000px",borderBottom:'0px solid black'}} image={require("assets/img/2.jpg")} >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      
      {matches.small && <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"Black", paddingLeft:"40px",paddingRight:"40px",margin:"4px"}}>
        <br></br>
        <GridContainer>
          {ShowRegistryComponent}
        </GridContainer>
        < hr style={{width:"55%"}} ></hr>
      </div>}

      {matches.medium && <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"Black",paddingLeft:"40px",paddingRight:"40px",margin:"4px"}}>
        <br></br>
        <GridContainer>
          {ShowRegistryComponent}
        </GridContainer>
        < hr style={{width:"55%"}} ></hr>
      </div>}

      {matches.large && <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"transparent",paddingTop:"200px", paddingLeft:"80px",paddingRight:"80px",borderRadius:"38px",borderTop:"2px black darkgrey",top:'50%'}}>
        <br></br>
        <GridContainer>
          {ShowRegistryComponent}
        </GridContainer>
  
      </div>}

      <Footer />
    </div>
    )}
    </Media>
  );
}