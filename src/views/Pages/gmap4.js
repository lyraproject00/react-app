import React, { Component } from "react";

import { Map, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';

class Gmap4 extends Component {
    state = {
      activeMarker: {},
      selectedPlace: {},
      showingInfoWindow: false
    };
  
    onMarkerClick = (props, marker) =>
      this.setState({
        activeMarker: marker,
        selectedPlace: props,
        showingInfoWindow: true
      });
  
    onInfoWindowClose = () =>
      this.setState({
        activeMarker: null,
        showingInfoWindow: false
      });
  
    onMapClicked = () => {
      if (this.state.showingInfoWindow)
        this.setState({
          activeMarker: null,
          showingInfoWindow: false
        });
    };
  
    render() {
      if (!this.props.loaded) return <div>Loading...</div>;
  
      return (
        <Map
          className="map"
          google={this.props.google}
          onClick={this.onMapClicked}
          style={{ height: '100%', position: 'relative', width: '100%' }}
          zoom={14}>
          <Marker
            name="SOMA"
            onClick={this.onMarkerClick}
            position={{ lat: 37.778519, lng: -122.40564 }}
          />
  
          <Marker
            name="Dolores park"
            onClick={this.onMarkerClick}
            position={{ lat: 37.759703, lng: -122.428093 }}
          />
  
          <Marker name="Current location" onClick={this.onMarkerClick} />
  
          <InfoWindow
            marker={this.state.activeMarker}
            onClose={this.onInfoWindowClose}
            visible={this.state.showingInfoWindow}>
            <div>
              <h1>{this.state.selectedPlace.name}</h1>
            </div>
          </InfoWindow>
  
        </Map>
      );
    }
  }
  
  export default GoogleApiWrapper({
    apiKey: 'AIzaSyA8k2utFV196NDfL2ioPEkta0DGgMF9i0I'
  })(Gmap4);