import React from "react"
import Button from "components/CustomButtons/Button.js";
import GridItem from "components/Grid/GridItem.js";
import Media from 'react-media';
import classNames from "classnames";
import { Container, Format } from '@atlaskit/badge';
import { withTranslation } from 'react-i18next';
import brflag from 'assets/img/brfilter.png'
import enflag from 'assets/img/enfilter.png'
import enbrflag from 'assets/img/enbrfiltered.png'

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/views/componentsSections/completedStyle.js";

import ShowTag2 from "./tags2.js";
import { isPropertySignature } from "typescript";
import { borderRadius } from "@atlaskit/theme";

const useStyles = makeStyles(styles);

function ShowRegistry(props) {
    const classes = useStyles();
    console.log(props.item)
    let item = [props.item]
    let ENtitles = item.reduce((map, obj) => (map[obj.id] = obj.ENtitle, map), {});
    let BRtitles = item.reduce((map, obj) => (map[obj.id] = obj.BRtitle, map), {});

    let link = "registry3/" + props.id
    let res = `titles.${props.id}`
    let title
    let flag
    let lng
    
    switch (document.cookie) {
    case 'lng=en':
        lng = 'en'
        break;
    case 'lng=pt_br':
        lng = 'pt_br'
        break;
    }

    lng= lng ? lng : 'en'
    
    const t = (key) => {
        var v = null;
        if (lng == 'pt_br') {
            v = BRtitles[key];
        }
        if (lng == 'en'){
           v = ENtitles[key];
        }
        return v;
     };

    switch (props.language) {
        case 'por':
            title = props.BRtitle
            flag = brflag
            //2console.log('portuguese');
            break;
        case 'eng':
            title = props.ENtitle
            flag = enflag
            //console.log('english');
            break;
        case 'eng/por':
            title = t(res)
            flag = enbrflag
            //console.log('both');
            break;
    }
    
    return (
        <Media queries={{
            small: "(max-width: 599px)",
            medium: "(min-width: 600px) and (max-width: 1199px)",
            large: "(min-width: 1200px)"
          }}>
              {matches => (
        
        
            <GridItem xs={12} sm={12} md={6}  >
           
            <Button
                color="transparent"
                size="xl"
                href={link}
                target="_blank"
                rel="noopener noreferrer"
                
            >   
                <div style={{paddingTop:"4px",paddingBottom:"4px",paddingLeft:"4px",position:"relative",border: '0px solid black', backgroundColor: 'rgba(10, 10, 10, 0.3)',borderRadius: '28px',marginLeft:"22px",marginRight:"22px",marginBottom:"10px"}}>
                {matches.small && <img src={props.img} alt="front" width= "35%" height= "120px" style={{borderRadius: '28px'}}></img>}
                {matches.medium && <img src={props.img} alt="front" width= "40%" height= "150px" style={{borderRadius: '28px'}}></img>} 
                {matches.large && <img src={props.img} alt="front" width= "45%" height= "200px" style={{borderRadius: '28px'}} ></img>}
                
                {matches.small && <span style ={{whiteSpace:"pre-line",verticalAlign:"middle", textAlign: "left",display: "inline-block",width:"50%",fontFamily: "Courier New",paddingLeft:"10px",paddingTop:"20px",fontSize: "50%",position:"absolute",color:"rgba(190,190,190)"}}>{title} test ovni test nave</span>}
                {matches.medium && <span style ={{whiteSpace:"pre-line",verticalAlign:"middle", textAlign: "left",display: "inline-block",width:"50%",fontFamily: "Courier New",paddingLeft:"10px",paddingTop:"30px",fontSize: "60%",position:"absolute",color:"rgba(190,190,190)"}}>{title} test ovni test nave</span>}
                {matches.large && <span  style ={{whiteSpace:"pre-line",verticalAlign:"middle", textAlign: "left",display: "inline-block",width:"48%",fontFamily: "Courier New",paddingLeft:"18px",paddingTop:"40px",fontSize: "70%",position:"absolute",color:"rgba(190,190,190)"}}>{title} test ovni test nave </span>}
                
                <ShowTag2 item={props.tr} />
                
                {matches.small && <span ><img src={flag} style={{marginLeft:'48%',height:'20px',marginTop:'130px',display:'inline-block'}} ></img> </span>}
                {matches.medium && <span><img src={flag} style={{marginLeft:'48%',height:'20px',marginTop:'130px',display:'inline-block'}} ></img> </span>}
                {matches.large && <span ><img src={flag} style={{marginLeft:'48%',height:'20px',marginTop:'130px',display:'inline-block'}} ></img> </span>}

                </div>
            </Button>
            <br/><br/>

            </GridItem>
           
            )}
            </Media>
    )
}
// marginRight:18px
//<p style={{paddingLeft:"5px",fontSize: "70%"}}>aaaaaaaa</p>
// < hr style={{width:"0%"}} ></hr>

export default ShowRegistry         