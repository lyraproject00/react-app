import React from "react"
import { withTranslation } from 'react-i18next';
import deepblue from 'assets/img/point.png'
import green from 'assets/img/green.png'
import red from 'assets/img/darkred.png'
import lightblue from 'assets/img/lightblue.png' 
import grey from 'assets/img/grey.png'
import flag from 'assets/img/br32p.png'

var image = ""
var TagName = ""

function ShowTag(props) {
    const { t } = props;
    switch(props.item){
        case "CE":
            image = deepblue
            TagName = t("t1")
            break;
        case "HA":
            image = green
            TagName = t("t2")
            break;
        case "MS":
            image = red
            TagName = t("t3")
            break;
        case "DC":
            image = lightblue
            TagName = t("t4")
            break;
        case "OT":
            image = grey
            TagName = t("t5")
            break;
    }
      
    return (
        <span><span style={{verticalAlign:"50%",whiteSpace:"pre-line",display:'inline-block',marginLeft:"15px",position:"absolute",top:"60%",fontFamily:"Brush Script MT",color:"rgba(150,150,150)",fontSize: "50%",textTransform:'lowercase',marginTop:'30px',backgroundColor:'rgba(10, 10, 10, 0)',padding:'5px',paddingRight:'10px',paddingLeft:'8px',borderRadius:'20px',width:'50%'}}><img src={image} style={{borderRadius:'18px',backgroundColor:'rgba(10, 10, 10, 0.1)',height:'5px'}}></img><span style={{marginLeft:"10px"}}></span>{TagName}</span></span>
        
        )
}

export default withTranslation()(ShowTag);

//height:10 paddingleft:20