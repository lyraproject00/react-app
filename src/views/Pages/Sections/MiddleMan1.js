import React, { useState, useEffect } from "react";
import styles from "assets/jss/material-kit-react/views/landingPage.js";
import { makeStyles } from "@material-ui/core/styles";

import ButtonsRegistries5 from "./ButtonsRegistries5.js";

import classNames from "classnames";
import Spinner from '@atlaskit/spinner';

import GridContainer from "components/Grid/GridContainer.js";
const useStyles = makeStyles(styles);

class MiddleMan extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        username: '',
        age: null,
      };
    }
    shuffle = (array) => {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }  

    handleChange(event) {   
         value1 = event.target.value; 
        }
    
    handleChange1(event) {   
        select1 = event.target.value; 
        }
    
    handleChange2(event) {   
        select2 = event.target.value; 
        }        
    
    handleChange3(event) {   
        select3 = event.target.value; 
        }

    handleSubmit(event) {
        event.preventDefault();
        SetV(value1)
        Sets1(select1)
        //Sets2(select2)
        Sets3(select3)
        
         
        if (v != undefined)
            v = [v]
            //item = item.filter(registry => v.includes(registry.ENtitle))
            //item = item.filter(registry => v.includes(registry.BRtitle))
            console.log(v)
            
        if (s1)
            console.log(select1)
        //if (s2)
        //    console.log(select2)
        if (s3)
            console.log(select3)

  
    }
    shuffle(item);   
    start(item) {
        let ShowRegistryComponent = item.map(item => <ButtonsRegistries5 key={item.id} language={item.language} id={item.id} v={s1 ? s1 : 'None'} ENtitle={item.ENtitle} BRtitle={item.BRtitle} tr={item.type_of_registry} img={item.images[0].image} />);
    }
    
    start();
    render() {
    return(
        <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"transparent",paddingTop:"100px", paddingLeft:"90px",paddingRight:"90px",borderRadius:"38px",borderTop:"2px black darkgrey",top:'50%'}}>
        <div style={{textAlign:'center'}}>
          <form onSubmit={handleSubmit} style={{paddingBottom:'60px'}}>
            <input type="text" placeholder="Search.." onChange={handleChange} value={value1} style={{background:'rgba(0,0,0,0.3)',width:'600px',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',color:"lightgrey",fontFamily: "Courier New",paddingLeft:'10px'}} />
            <select id="types" name="types" onChange={handleChange1} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} selected="selected">Types</option>
              <option value="CE">Close Encounters</option>
              <option value="HA">History/Archeology</option>
              <option value="MS">Miscellaneous</option>
              <option value="DC">Documentary</option>
              <option value="OT">Others</option>
            </select>
            {/*<select id="tags" name="tags" onChange={handleChange2} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} selected="selected">Tags</option>
              <option value="volvo">Volvo</option>
              <option value="saab">Saab</option>
              <option value="fiat">Fiat</option>
              <option value="audi">Audi</option>
            </select>*/}
            <select id="tags" name="tags" onChange={handleChange3} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} selected="selected">Lng</option>
              <option value="volvo">Eng</option>
              <option value="saab">Pt-Br</option>
            
            </select>
            <input type="submit"  value="   Send   " style={{background:'rgba(0,0,0,0.7)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',color:"rgba(100,100,100,1)",fontFamily: "Courier New",cursor:'pointer'}}></input> 
          </form>
        </div>
        
        <GridContainer>
            {ShowRegistryComponent}
        </GridContainer>
  
    </div>   
    )
}
}
export default MiddleMan