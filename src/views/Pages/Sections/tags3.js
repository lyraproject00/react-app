import React from "react"

import deepblue from 'assets/img/point.png'
import green from 'assets/img/green.png'
import red from 'assets/img/darkred.png'
import lightblue from 'assets/img/lightblue.png' 
import grey from 'assets/img/grey.png'
import flag from 'assets/img/br32p.png'

var image = ""
var TagName = ""

function ShowTag(props) {
    switch(props.item){
        case "CE":
            image = deepblue
            TagName = "Close Enconter"
            break;
        case "HA":
            image = green
            TagName = "History/Archeology"
            break;
        case "MS":
            image = red
            TagName = "Miscellaneous"
            break;
        case "DC":
            image = lightblue
            TagName = "Documentary"
            break;
        case "OT":
            image = grey
            TagName = "Other"
            break;
    }
      
    return (
        <div style ={{whiteSpace:"pre-line",display:'block'}}><span style={{verticalAlign:"50%",whiteSpace:"pre-line",display:'inline-block',marginLeft:"20px",position:"absolute",top:"60%",fontFamily:"Brush Script MT",color:"rgba(150,150,150)",fontSize: "50%",textTransform:'lowercase',marginTop:'30px',backgroundColor:'rgba(10, 10, 10, 0.5)',padding:'5px',paddingRight:'10px',paddingLeft:'8px',borderRadius:'20px'}}><img src={image} style={{borderRadius:'18px',backgroundColor:'rgba(10, 10, 10, 0.1)',height:'5px'}}></img><span style={{marginLeft:"10px"}}></span>{TagName}</span><span style ={{whiteSpace:"pre-line",display:'inline-block',marginLeft:'50px'}}></span></div>
        
        )
}

export default ShowTag;

//height:10 paddingleft:20