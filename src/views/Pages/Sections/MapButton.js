import React from "react"
import { withTranslation } from 'react-i18next';

import {
    AwesomeButton
  } from 'react-awesome-button';
  
//https://lyraproject.vercel.app/gmap2
//http://localhost:3000/gmap2
function MapButton(props) {
    const { t } = props;
    if (props.items.localizations[0] != undefined){
    return (
        <div style={{textAlign:"center"}}><AwesomeButton type="primary" href={"https://lyraproject.vercel.app/gmap2/"+props.items.localizations[0].lat+"/"+props.items.localizations[0].lng} target="_blank"><img src={require("assets/img/gicon3edited3.jpg")} width={"40px"} height={"40px"} alt="icon"></img>{t('mapa')}</AwesomeButton></div>
        
    )
    }
    else{
    return (
        <div></div>
    )
    }
}

export default withTranslation()(MapButton);