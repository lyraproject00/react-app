import React, { useState, useEffect } from "react";
import styles from "assets/jss/material-kit-react/views/landingPage.js";
import { makeStyles } from "@material-ui/core/styles";

import ButtonsRegistries5 from "./ButtonsRegistries5.js";

import classNames from "classnames";
import Spinner from '@atlaskit/spinner';

import GridContainer from "components/Grid/GridContainer.js";
const useStyles = makeStyles(styles);

function MiddleMan(props) {
    let itemsprev = props.item
    const classes = useStyles();
    let value1, select1,select2,select3
    let items = []
    let [item, setItem] = useState();
    let [v, setv] = useState();
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }  

    function handleChange(event) {
        value1 = event.target.value; 
        //setv(event.target.value);
          console.log(value1) 
        }
    
    function handleChange1(event) {   
        select1 = event.target.value; 
        }
    
    function handleChange2(event) {   
        select2 = event.target.value; 
        }        
    
    function handleChange3(event) {   
        select3 = event.target.value; 
        }

    function handleSubmit(event) {
      //setv(value1)
      console.log('v',value1)
      
      //if (value1 === '<empty string>'){
      //  console.log('triggered')
      //}

      if ((value1 !== undefined) && (select1 === undefined) && ((select3 === undefined))){
        //itemsprev.filter((registry) => registry.BRtitle ? registry.BRtitle.includes(value1) : null)
        items = itemsprev.filter((registry) => (registry.ENtitle ? registry.ENtitle.includes(value1) : false) || (registry.BRtitle ? registry.BRtitle.includes(value1) : false))
      
      }
      if ((value1 === undefined) && (select1 !== undefined) && ((select3 === undefined))){
        items = itemsprev.filter((registry) => registry.type_of_registry == select1)
        
      }
      if ((value1 === undefined) && (select1 === undefined) && ((select3 !== undefined))){
        items = itemsprev.filter((registry) => registry.language == select3 )
          
      }
      if ((value1 !== undefined) && (select1 !== undefined) && ((select3 === undefined))){
        items = itemsprev.filter((registry) => ((registry.ENtitle ? registry.ENtitle.includes(value1) : false) || (registry.BRtitle ? registry.BRtitle.includes(value1) : false)) && (registry.type_of_registry == select1))
        
      }
      if ((value1 === undefined) && (select1 !== undefined) && ((select3 !== undefined))){
        items = itemsprev.filter((registry) => (registry.type_of_registry == select1) && (registry.type_of_registry == select3 ))
        
      }
      if ((value1 !== undefined) && (select1 === undefined) && ((select3 !== undefined))){
        items = itemsprev.filter((registry) => ((registry.ENtitle ? registry.ENtitle.includes(value1) : false) || (registry.BRtitle ? registry.BRtitle.includes(value1) : false)) && (registry.language == select3))
          
      }
      if ((value1 !== undefined) && (select1 !== undefined) && ((select3 !== undefined))){
        items = itemsprev.filter((registry) => ((registry.ENtitle ? registry.ENtitle.includes(value1) : false) || (registry.BRtitle ? registry.BRtitle.includes(value1) : false)) && (registry.language == select1) && (registry.language == select3) )
        
      }
      if ((value1 === undefined) && (select1 === undefined) && ((select3 === undefined))){
        items = itemsprev
          
      }
      
      console.log(items)   
      setItem(items)    
      event.preventDefault();

    }   

    shuffle(itemsprev);
    let ShowRegistryComponent = item ? item.map(item => <ButtonsRegistries5 key={item.id} language={item.language} id={item.id} v={'Passou'} ENtitle={item.ENtitle} BRtitle={item.BRtitle} tr={item.type_of_registry} img={item.images[0].image} />) : itemsprev.map(item => <ButtonsRegistries5 key={item.id} language={item.language} id={item.id} v={'None'} ENtitle={item.ENtitle} BRtitle={item.BRtitle} tr={item.type_of_registry} img={item.images[0].image} />);

    return(
        <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"transparent",paddingTop:"100px", paddingLeft:"90px",paddingRight:"90px",borderRadius:"38px",borderTop:"2px black darkgrey",top:'50%'}}>
        <div style={{textAlign:'center'}}>
          <form onSubmit={handleSubmit} style={{paddingBottom:'60px'}}>
            <input type="text" id="search" placeholder="Search.." onChange={handleChange} value={value1} style={{background:'rgba(0,0,0,0.3)',width:'600px',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',color:"lightgrey",fontFamily: "Courier New",paddingLeft:'10px'}} />
            <select id="types" name="types" onChange={handleChange1} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} selected="selected">Types</option>
              <option value={undefined}></option>
              <option value="CE">Close Encounters</option>
              <option value="HA">History/Archeology</option>
              <option value="MS">Miscellaneous</option>
              <option value="DC">Documentary</option>
              <option value="OT">Others</option>
            </select>
            {/*<select id="tags" name="tags" onChange={handleChange2} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} selected="selected">Tags</option>
              <option value="volvo">Volvo</option>
              <option value="saab">Saab</option>
              <option value="fiat">Fiat</option>
              <option value="audi">Audi</option>
            </select>*/}
            <select id="lng" name="lng" onChange={handleChange3} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} selected="selected">Lng</option>
              <option value={undefined}></option>
              <option value="eng">Eng</option>
              <option value="por">Pt-Br</option>
              <option value="eng/por">eng and br</option>
            
            </select>
            <input type="submit"  value="   Send   " style={{background:'rgba(0,0,0,0.7)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',color:"rgba(100,100,100,1)",fontFamily: "Courier New",cursor:'pointer'}}></input> 
          </form>
        </div>
        
        <GridContainer>
            {ShowRegistryComponent}
        </GridContainer>
  
    </div>   
    )
}
export default MiddleMan