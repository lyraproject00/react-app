import React from "react"
import Button from "components/CustomButtons/Button.js";
import GridItem from "components/Grid/GridItem.js";
import Media from 'react-media';
import classNames from "classnames";

import { P300, N0 } from '@atlaskit/theme/colors';

import { Container, Format } from '@atlaskit/badge';
import { withTranslation } from 'react-i18next';

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/views/componentsSections/completedStyle.js";

import ShowTag from "./tags.js";
import { isPropertySignature } from "typescript";

const useStyles = makeStyles(styles);

function ShowRegistry(props) {
    const classes = useStyles();
    const { t } = props;
    

    let link = "http://localhost:3000/registry/" + props.id
    let res = `titles.${props.id}`
    let title
    switch (props.language) {
        case 'por':
            title = props.BRtitle
            console.log('portuguese');
            break;
        case 'eng':
            title = props.ENtitle
            console.log('english');
            break;
        case 'eng/por':
            title = t(res)
            console.log('both');
            break;
    }
    
    return (
        <Media queries={{
            small: "(max-width: 599px)",
            medium: "(min-width: 600px) and (max-width: 1199px)",
            large: "(min-width: 1200px)"
          }}>
              {matches => (
        
        
            <GridItem xs={12} sm={12} md={6}  >
           
            <Button
                color="transparent"
                size="xl"
                href={link}
                target="_blank"
                rel="noopener noreferrer"
                
            >   
                <div style={{paddingTop:"4px",paddingBottom:"4px",paddingLeft:"4px",position:"relative",border: '3px solid black', backgroundColor: 'rgb(7, 7, 7)',borderRadius: '28px',marginLeft:"8px",marginRight:"8px",marginBottom:"10px"}}>
                {matches.small && <img src={props.img} alt="front" width= "60%" height= "180px" ></img>}
                {matches.medium && <img src={props.img} alt="front" width= "50%" height= "200px"></img>} 
                {matches.large && <img src={props.img} alt="front" width= "50%" height= "200px" style={{borderRadius: '28px'}} ></img>}
                
                {matches.small && <span style ={{whiteSpace:"pre-line", textAlign: "left",display: "inline-block",width:"30%",height:"40px",bottom: '60px',fontFamily: "Brush Script MT",paddingLeft:"20px",fontSize: "60%"}}>{title} </span>}
                {matches.medium && <span style ={{whiteSpace:"pre-line", textAlign: "left",display: "inline-block",width:"30%",height:"40px",bottom: '60px',fontFamily: "Brush Script MT",paddingLeft:"20px",fontSize: "75%"}}>{title}</span>}
                {matches.large && <span  style ={{whiteSpace:"pre-line",verticalAlign:"middle", textAlign: "left",display: "inline-block",width:"48%",fontFamily: "Courier New",paddingLeft:"18px",paddingTop:"40px",fontSize: "70%",position:"absolute",color:"lightgrey"}}>{title} test ovni test nave ceu test</span>}
                <ShowTag item={props.tr} />

                </div>
            </Button>
            <br/><br/>

            </GridItem>
           
            )}
            </Media>
    )
}
// marginRight:18px
//<p style={{paddingLeft:"5px",fontSize: "70%"}}>aaaaaaaa</p>
// < hr style={{width:"0%"}} ></hr>

export default withTranslation()(ShowRegistry);         