import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import { useKeenSlider } from "keen-slider/react"
import "keen-slider/keen-slider.min.css"
import "./styles.css"
import styles from "assets/jss/material-kit-react/views/componentsSections/completedStyle.js";
import ReactPlayer from 'react-player'

const useStyles = makeStyles(styles);

export default function SectionCompletedExamples(props) {
    const listimages = props.img.map((image)=>
    <div>
    <img src={image.image} alt="x" className="slick-image" />
    </div>
    );

  const classes = useStyles();
  const [sliderRef] = useKeenSlider({ loop: true })

  return (
    
       
            <div>
            <div style={{float:"left"}}>
            
          <h4 style={{width:"500px",height:"200px"}}>
          The kit comes with three pre-built pages to help you get started
            faster. You can change the text and images and you{"'"}re good to
            go. More importantly, looking at them will give you a picture of
            what you can build with this powerful kitaaaa aaa aaaa aa aaaaa aa aaa aa aaaa aaa aaa
            aaa aaaaa aaa aaa aaaaaa aaaaaa aa aaaaaaa aaaaaa aaa aaaa aa aa aaa aaaaa aa aaaaaaaaaaaa a aaaaa  a
             a aaaaa a  aa a  a aa a a a a aaa a a a a a a a a a a a a a a a a  a a a a a a a a a a aaaaaa a  aa a a  a
             a a a a a a a a a a a a a a  a a a aaaaaaaaaaa a a a a a a a bb b b
          </h4>
          </div>

      
      <div ref={sliderRef} className="keen-slider"  style={{width :"100px",float:"right"}}>
      <div className="keen-slider__slide number-slide1">1</div>
      <div className="keen-slider__slide number-slide2">2</div>
      <div className="keen-slider__slide number-slide3">3</div>
      <div className="keen-slider__slide number-slide4">4</div>
      <div className="keen-slider__slide number-slide5">5</div>
      <div className="keen-slider__slide number-slide6">6</div>
  
    </div>

    <ReactPlayer width="50%" height="300px" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />

  </div>

  );
}
