import React, { useState, useEffect } from "react";
import styles from "assets/jss/material-kit-react/views/landingPage.js";
import { makeStyles } from "@material-ui/core/styles";

import ButtonsRegistries5 from "./ButtonsRegistries5.js";

import classNames from "classnames";
import Spinner from '@atlaskit/spinner';
import { withTranslation } from 'react-i18next';
import GridContainer from "components/Grid/GridContainer.js";
const useStyles = makeStyles(styles);

function MiddleMan(props) {
    const { t } = props;
    let itemsprev = props.item
    const classes = useStyles();
    let Search = React.createRef();
    let select1 = React.createRef();
    let select2 = React.createRef();
    let select3 = React.createRef();
    let items = []
    let [item, setItem] = useState();
    let [v, setv] = useState();
    
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }  

    function handleSubmitest (event) {
      event.preventDefault();
      //alert(.current.value);
    }
    
    function handleSubmit(event) {
      let Searchv = new String(Search.current.value)
      let select1v = new String(select1.current.value)
      let select3v = new String(select3.current.value)
      console.log('v',Searchv)
      console.log('v',select1v)
      console.log('v',select3v)

      if ((Searchv != "") && (select1v == "") && (select3v == "")){
        //itemsprev.filter((registry) => registry.BRtitle ? registry.BRtitle.includes(value1) : null)
        items = itemsprev.filter((registry) => (registry.ENtitle ? registry.ENtitle.includes(Searchv) : false) || (registry.BRtitle ? registry.BRtitle.includes(Searchv) : false))
      
      }
      if ((Searchv == "") && (select1v != "") && (select3v == "")){
        items = itemsprev.filter((registry) => registry.type_of_registry == select1v )
        
      }
      if ((Searchv == "") && (select1v == "") && (select3v != "")){
        items = itemsprev.filter((registry) => registry.language == select3v )
          
      }
      if ((Searchv != "") && (select1v != "") && (select3v == "")){
        items = itemsprev.filter((registry) => ((registry.ENtitle ? registry.ENtitle.includes(Searchv) : false) || (registry.BRtitle ? registry.BRtitle.includes(Searchv) : false)) && (registry.type_of_registry == select1v))
        
      }
      if ((Searchv == "") && (select1v != "") && (select3v != "")){
        items = itemsprev.filter((registry) => (registry.type_of_registry == select1) && (registry.language == select3 ))
        
      }
      if ((Searchv != "") && (select1v == "") && (select3v != "")){
        items = itemsprev.filter((registry) => ((registry.ENtitle ? registry.ENtitle.includes(Searchv) : false) || (registry.BRtitle ? registry.BRtitle.includes(Searchv) : false)) && (registry.language == select3v))
          
      }
      if ((Searchv != "") && (select1v != "") && (select3v != "")){
        console.log('foi')
        items = itemsprev.filter((registry) => ((registry.ENtitle ? registry.ENtitle.includes(Searchv) : false) || (registry.BRtitle ? registry.BRtitle.includes(Searchv) : false)) && (registry.type_of_registry == select1v) && (registry.language == select3v) )
        
      }
      if ((Searchv == "") && (select1v == "") && (select3v == "")){
        items = itemsprev
          
      }
      
      console.log(items)       
      event.preventDefault();
      setItem(items)
      console.log(itemsprev)    
    }   

    shuffle(itemsprev);
    let ShowRegistryComponent = item ? item.map(item => <ButtonsRegistries5 key={item.id} language={item.language} id={item.id} marginb={props.marginb} ENtitle={item.ENtitle} BRtitle={item.BRtitle} tr={item.type_of_registry} img={item.images[0].image} />) : itemsprev.map(item => <ButtonsRegistries5 key={item.id} language={item.language} id={item.id} marginb={props.marginb} ENtitle={item.ENtitle} BRtitle={item.BRtitle} tr={item.type_of_registry} img={item.images[0].image} />);

    return(
        <div className={classNames(classes.main, classes.mainRaised)} style={{background:'rgba(0,0,0,0)',paddingTop:props.pt, paddingLeft:props.one,paddingRight:props.one,borderRadius:"38px",borderTop:"2px black darkgrey",top:'50%'}}>
        <div style={{textAlign:'center'}}>
          <form onSubmit={handleSubmit} style={{paddingBottom:props.pb}}>
            <input type="text" id="search" placeholder={t('search')} ref={Search}  style={{background:'rgba(0,0,0,0.3)',width:props.sew,outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '130%',height:'40px',border:'0',borderRadius:'20px',color:"rgba(200,200,200,0.9)",fontFamily: "Courier New",paddingLeft:'10px'}} />
            {/*  <input type="text" id="search" placeholder="Search.." ref={Search}  style={{background:'rgba(0,0,0,0.3)',width:'600px',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',color:"lightgrey",fontFamily: "Courier New",paddingLeft:'10px'}} /> */}
            <select id="types" name="types" ref={select1} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '130%',bottom:'10px',height:'40px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,1)",fontFamily: "Courier New",width:props.sw,textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} >{t("types")}</option>
              <option value={undefined}></option>
              <option value="CE">{t("t1")}</option>
              <option value="HA">{t("t2")}</option>
              <option value="MS">{t("t3")}</option>
              <option value="DC">{t("t4")}</option>
              <option value="OT">{t("t5")}</option>
            </select>
            {/*<select id="tags" name="tags" onChange={handleChange2} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}>
              <option value="" style={{display: "none"}} selected="selected">Tags</option>
              <option value="volvo">Volvo</option>
              <option value="saab">Saab</option>
              <option value="fiat">Fiat</option>
              <option value="audi">Audi</option>
            </select>*/}
            <select id="lng" name="lng" ref={select3} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '130%',bottom:'10px',height:'40px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:props.sw,textAlign:'center',cursor:'pointer'}}>
              {/*<select id="lng" name="lng" ref={select3} style={{background:'rgba(0,0,0,0.3)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '30px',border:'0',borderRadius:'20px',paddingLeft:'10px',color:"rgba(1,1,1,0.98)",fontFamily: "Courier New",width:'150px',textAlign:'center',cursor:'pointer'}}> */}
              <option value="" style={{display: "none"}} >Lng</option>
              <option value={undefined}></option>
              <option value="eng/por">{t("EaB")}</option>
              <option value="eng">Eng</option>
              <option value="por">Pt-Br</option>
              
            </select>
            <input type="submit"  value={t("send")} style={{background:'rgba(0,0,0,0.7)',outline:'none',border: '2px solid #fff',boxSizing:'border-box',fontSize: '130%',height:'40px',border:'0',borderRadius:'20px',color:"rgba(150,150,150,0.9)",fontFamily: "Courier New",cursor:'pointer',width:props.sw,textAlign:'center',boxSizing:'border-box'}}></input> 
          </form>
        </div>
        
        <GridContainer>
            {ShowRegistryComponent}
        </GridContainer>
  
    </div>   
    )
}
export default  withTranslation()(MiddleMan);