import React from "react"

import { G500, P300, N0, N900, R500, Y200, T300, N700} from '@atlaskit/theme/colors';

import { Container, Format } from '@atlaskit/badge';

var BColor = ""
var TColor = ""
var TagName = ""

function ShowTag(props) {
    switch(props.item){
        case "CE":
            BColor = P300
            TColor = N0
            TagName = "Close Enconter"
            break;
        case "HA":
            BColor = Y200
            TColor = N900
            TagName = "History/Archeology"
            break;
        case "MS":
            BColor = R500
            TColor = N0
            TagName = "Miscellaneous"
            break;
        case "CC":
            BColor = G500
            TColor = N0
            TagName = "Crop Circle"
            break;
        case "DC":
            BColor = T300
            TColor = N900
            TagName = "Documentary"
            break;
        case "OT":
            BColor = N700
            TColor = N0 
            TagName = "Other"
            break;
    }
      
    return (
        <span style={{verticalAlign:"50%",paddingLeft:"20px",height:"30px",position:"absolute",top:"70%"}}><Container backgroundColor={BColor} textColor={TColor}>
                                                                                 <Format>{TagName}</Format>
                                                                        </Container></span>
    )
}

export default ShowTag;

//height:10 paddingleft:20