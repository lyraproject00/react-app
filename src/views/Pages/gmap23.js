import React, { Component } from "react";
// @material-ui/core components
import { withRouter } from "react-router";

import { Map, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';
import { withTranslation } from 'react-i18next';
import classNames from "classnames";

//import Marker from 'react-leaflet-enhanced-marker'
// @material-ui/icons
import axios from "axios";

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";

import brflag from 'assets/img/brfilter.png'
import enflag from 'assets/img/enfilter.png'
import enbrflag from 'assets/img/enbrfiltered2.png'
import TestComponent from "views/Components/Sections/TestComponent.js";
import { string } from "prop-types";

const dashboardRoutes = [];

const mapStyles = {
  width: '100%',
  height: '100%',
};

//"http://localhost:8000/api/registry/"
  //"https://lyraproject0.herokuapp.com/api/registry/"

export class Gmap2 extends Component {
    state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      registries_list : [],
      Loading : false
    };
   
    componentDidMount() {
        this.refreshRList();
      }

    refreshRList = () => {
    axios
        .get("https://lyraproject0.herokuapp.com/api/registry/")
        .then(res => { this.setState({ registries_list: res.data }); 
        let ENtitles = this.state.registries_list.reduce((map, obj) => (map[obj.id] = obj.ENtitle, map), {});
        let ENdescription = this.state.registries_list.reduce((map, obj) => (map[obj.id] = obj.ENtext, map), {});
        let BRtitles = this.state.registries_list.reduce((map, obj) => (map[obj.id] = obj.BRtitle, map), {});
        let BRdescription = this.state.registries_list.reduce((map, obj) => (map[obj.id] = obj.BRtext, map), {});
        
        const resources= {
          en: {
            translation: {
              header1: "About",
              header2: "Database" ,
              header3: "Map",
              header4: "Pages",
              
            }
          },
          pt_br: {
            translation: {
              header1: "Sobre",
              header2: "Banco de dados",
              header3: "Mapa",
              header4: "Páginas",
                
            }
          }
        }
        let lng 
        let titles
        let texts
        switch (document.cookie) {
          case 'lng=en':
              lng = 'en'
              
              titles = ENtitles
              texts =  ENdescription
              break;
          case 'lng=pt_br':
              lng = 'pt_br'
              titles = BRtitles 
              texts = BRdescription
              break;
          }

        //let index = res.id - 1
        //let res1 = resources.en.translation.Registry[index].title
        //console.log(res1)
        i18n
          // detect user language
          // learn more: https://github.com/i18next/i18next-browser-languageDetector
          // pass the i18n instance to react-i18next.
          .use(initReactI18next)
          // init i18next
          // for all options read: https://www.i18next.com/overview/configuration-options
          .init({
            resources,
            //detection: options,
            lng: lng ? lng : 'en',
            debug: true,
            fallbackLng: 'en',
            supportedLngs: ['en', 'pt_br'],
            interpolation: {
              escapeValue: false, // not needed for react as it escapes by default
            },
        
            
          });
          this.setState({
            Loading: true,
           
          });
        })
        .catch(err => console.log(err));
    };

    onMarkerClick = (props, marker) =>
      this.setState({
        activeMarker: marker,
        selectedPlace: props,
        showingInfoWindow: true
      });

    onInfoWindowClose = () =>
      this.setState({
        activeMarker: null,
        showingInfoWindow: false
      });
   
    onMapClicked = () => {
      if (this.state.showingInfoWindow)
        this.setState({
          activeMarker: null,
          showingInfoWindow: false
        });
    };  
    
    render() {
        const MarkersComponent = this.state.registries_list.map(item => {
          if (item.localizations[0] == undefined) {
            return false; // skip
          }
          
      
          let description,title,titles
          switch (item.language) {
            case 'por':
                title = item.BRtitle
                description = item.BRdescription
                //flag = brflag
              
                break;
            case 'eng':
                title = item.ENtitle
                //description = item.ENdescription
                //flag = enflag
                //console.log('english');
                break;
            case 'eng/por':
              if (titles == undefined){
                title = 'a'
              }
              else{
              title = titles[item.id] ? titles[item.id] : 'a'
              //description = t(res2)
              //flag = enbrflag
              //console.log('both');
              }
              break;
        }
          
          console.log(title)
          return <Marker
          id={item.id}
          title={title}
          //description={description}
          //flag={flag}
          onClick={this.onMarkerClick}
          position={{
              lat: item.localizations[0].lat,
              lng: item.localizations[0].lng
            }}
          icon={{
              url: require("assets/img/aliengreen3.png"),
              anchor: new this.props.google.maps.Point(15,15),
              scaledSize: new this.props.google.maps.Size(30,30)
            }}
            />;       
          });
        
    
        
      if(this.props.match.params.par2 == undefined){
      return (
        
        <div style={{backgroundColor:"rgb(1, 1, 1)"}}>
        <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Lyra Project"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
            height: 0,
            color: "dark"
            }}
    
        />
        <Parallax style={{height:"280px"}} filter image={require("assets/img/prism6.svg")}>
                
        </Parallax>
        
        <div  style={{backgroundColor:"black",paddingLeft:"0px",paddingRight:"0px"}}>
        <Map google={this.props.google}
            zoom={5}
            mapType="hybrid"
            initialCenter={{ lat: 38.8757676, lng: -99.0257517}}
            onClick={this.onMapClicked}>

        {MarkersComponent}
        
        <InfoWindow
            marker={this.state.activeMarker}
            onClose={this.onInfoWindowClose}
            visible={this.state.showingInfoWindow}>
            <div>
              <a href={"https://lyraproject.vercel.app/registry/"+this.state.selectedPlace.id}>{this.state.selectedPlace.title}</a>
            </div>
          </InfoWindow>

        </Map>
        
    
        </div>
        </div>
      )

    }else{
      
      return(
      <div style={{backgroundColor:"rgb(1, 1, 1)"}}>
      <Header
      color="transparent"
      routes={dashboardRoutes}
      brand="Lyra Project"
      rightLinks={<HeaderLinks />}
      fixed
      changeColorOnScroll={{
          height: 0,
          color: "dark"
          }}
  
      />
      <Parallax style={{height:"280px"}} filter image={require("assets/img/prism6.svg")}>
                
                </Parallax>
      
      <div  style={{backgroundColor:"black",paddingLeft:"0px",paddingRight:"0px"}}>
      <Map google={this.props.google}
          zoom={13}
          mapType="hybrid"
          initialCenter={{lat: this.props.match.params.par1, lng: this.props.match.params.par2}}
          onClick={this.onMapClicked}>

      {MarkersComponent}
      
      <InfoWindow
          marker={this.state.activeMarker}
          onClose={this.onInfoWindowClose}
          visible={this.state.showingInfoWindow}>
          <div>
            <a href={"https://lyraproject0.herokuapp.com/api/registry/"+this.state.selectedPlace.id}>{`${this.state.selectedPlace.name}\n${this.state.selectedPlace.description}`}</a>
          </div>
        </InfoWindow>

      </Map>
      
  
      </div>
      </div>

    )}

  }
}


export default withRouter(GoogleApiWrapper({
  apiKey: 'AIzaSyAyhjoF_UsnA9eQ7OcipwCRHa6G_pfk4uI'
})(Gmap2));

//{{ lat: 38.8757676, lng: -99.0257517}}
// initialCenter={{ lat: 38.8757676, lng: -99.0257517}}