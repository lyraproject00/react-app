import React, { useState, useEffect } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

import MiddleMan from './Sections/MiddleMan.js'
import background from 'assets/img/prism6.svg'

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import axios from "axios";
import Spinner from '@atlaskit/spinner';

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import Media from 'react-media';

import styles from "assets/jss/material-kit-react/views/landingPage.js";


const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  
  const [isLoading, setLoading] = useState(true);
  const [items, setItems] = useState();
  const [v, SetV] = useState();

  //"http://localhost:8000/api/registry/"
  //"https://lyraproject0.herokuapp.com/api/registry/"
  useEffect(() => {
    axios.get("https://lyraproject0.herokuapp.com/api/registry/").then(response => {
      console.log('a');
      
      setItems(response.data);
      setLoading(false);
    });
  }, []);

  

  if (isLoading) { 
     
    return(
      <div style={{backgroundImage: `url(${background})`}}>
          <Header
            color="transparent"
            routes={dashboardRoutes}
            brand="Lyra Project"
            rightLinks={<HeaderLinks />}
            fixed
            changeColorOnScroll={{
              height: 550,
              color: "dark"
              }}
              {...rest}
          />
          <Parallax filter style={{height:"1000px",borderBottom:'0px solid black',borderRadius:'200px'}} image={require("assets/img/2.jpg")}>
              <div className={classes.container}>
              <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                  </GridItem>
              </GridContainer>
              </div>
          </Parallax>
          <div className={classNames(classes.main, classes.mainRaised)} style={{backgroundColor:"transparent",paddingLeft:"0px",paddingRight:"0px",marginBottom:'100px'}}>
              
                  
                  <div style ={{textAlign: "center"}}>
                  <Spinner size="xlarge"/> <h1 style ={{display: "inline-block",paddingTop:"30px"}}>Loading Database...</h1><p></p>
                  </div>
                  
              
          </div>
          <Footer />
          </div>
        
  );
  
  }
  let ENtitles = items.reduce((map, obj) => (map[obj.id] = obj.ENtitle, map), {});
  let BRtitles = items.reduce((map, obj) => (map[obj.id] = obj.BRtitle, map), {});
 
  let lng
    
  const resources= {
      en: {
        translation: {
         titles: ENtitles
        }
      },
      pt_br: {
        translation: {
          titles: BRtitles   
        }
      }
    }

  switch (document.cookie) {
  case 'lng=en':
      lng = 'en'
      break;
  case 'lng=pt_br':
      lng = 'pt_br'
      break;
  }
  
  i18n
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
      resources,
      //detection: options,
      lng: lng ? lng : 'en',
      debug: true,
      fallbackLng: 'en',
      supportedLngs: ['en', 'pt_br'],
      interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
      },

  
});
  
  return (
    <Media queries={{
      small: "(max-width: 699px)",
      medium: "(min-width: 700px) and (max-width: 1299px)",
      large: "(min-width: 1300px)"
    }}>
        {matches => (
    <div style={{backgroundImage: `url(${background})`}}>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Lyra Project"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 550,
          color: "dark"
        }}
        {...rest}
      />
      <Parallax filter style={{height:"1000px",borderBottom:'0px solid black',borderRadius:'150px'}} image={require("assets/img/2.jpg")} >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
        
      {matches.small &&  <MiddleMan item={items} one='10px' sw='30%' sew='90%'/>}

      {matches.medium && <MiddleMan item={items} one='30px' sw='11%' sew='30%'/>}

      {matches.large && <MiddleMan item={items} one='85px' sw='8%' sew='30%'/>}
  
      <Footer />
    </div>
    )}
    </Media>
  );
  
}

//200px