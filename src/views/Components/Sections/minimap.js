import React, { Component } from "react";
// @material-ui/core components

import { Map, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';

const mapStyles = {
  width: '100px',
  height: '100px'
};

export class Minimap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      
    }
  }

    onMarkerClick = (props, marker) =>
      this.setState({
        activeMarker: marker,
        selectedPlace: props,
        showingInfoWindow: true
      });

    onInfoWindowClose = () =>
      this.setState({
        activeMarker: null,
        showingInfoWindow: false
      });
   
    onMapClicked = () => {
      if (this.state.showingInfoWindow)
        this.setState({
          activeMarker: null,
          showingInfoWindow: false
        });
    };
    
    
    render() {
       
      return (
        
        <Map google={this.props.google}
            zoom={8}
            mapType="hybrid"
            initialCenter={{ lat: this.props.lat, lng: this.props.lng}}
            onClick={this.onMapClicked}
            style={{width:"300px",height:"300px",position:"static"}}>
          <Marker
          id={1}
          name={this.props.title}
          onClick={this.onMarkerClick}
          
          position={{
            lat: this.props.lat,
            lng: this.props.lng
            }}>
        </Marker>

        </Map>
        
      )
    }
  }


export default GoogleApiWrapper({
  apiKey: 'AIzaSyA8k2utFV196NDfL2ioPEkta0DGgMF9i0I'
})(Minimap);
