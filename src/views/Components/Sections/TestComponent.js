
import { withTranslation } from 'react-i18next';

import brflag from 'assets/img/brfilter.png'
import enflag from 'assets/img/enfilter.png'
import enbrflag from 'assets/img/enbrfiltered2.png'

import {Marker} from 'google-maps-react';

function testcomponent(props) {
    const { t } = props;
    let title
    let res = `titles.${props.item.id}`
    

    switch (props.item.language) {
      case 'por':
          title = props.item.BRtitle
          
          //2console.log('portuguese');
          break;
      case 'eng':
          title = props.item.ENtitle
          
          //console.log('english');
          break;
      case 'eng/por':
          title = t(res)
          
          //console.log('both');
          break;
  }
    return (
        {title}
    )
           
    

   

}

export default withTranslation()(testcomponent);  