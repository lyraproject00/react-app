import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";

import styles from "assets/jss/material-kit-react/views/componentsSections/carouselStyle.js";

const useStyles = makeStyles(styles);

export default function SectionCarousel(props) {

    const listimages = props.img.map((image)=>
    <div>
    <img src={image.image} alt="x" className="slick-image" />
    </div>
    );

  const classes = useStyles();
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false
  };
  return (
    <div className={classes.section} style={{padding:"0px",paddingBottom:"5px",paddingTop:"40px"}}>
      <div className={classes.container}>
        <GridContainer>
          <GridItem xs={12} sm={8} md={6} className={classes.marginAuto}>
            <Card carousel>
              <Carousel {...settings}>
                {listimages}
              </Carousel>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
