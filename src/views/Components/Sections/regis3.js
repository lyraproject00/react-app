import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import { withTranslation } from 'react-i18next';
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import styles from "assets/jss/material-kit-react/views/componentsSections/completedStyle.js";

const useStyles = makeStyles(styles);

function SectionCompletedExamples(props) {
  const { t } = props;
  let title
  let text
  switch (props.item.language) {
    case 'por':
        title = props.item.BRtitle
        text = props.item.BRtext
        
        break;
    case 'eng':
        title = props.item.ENtitle
        text = props.item.ENtext
        
        break;
    case 'eng/por':
        title = t('title')
        text = t('text')
        break;
}
  console.log(text)
  const classes = useStyles();
  let newText = text.split('\n').map((item, i) => {
    return <p style={{fontSize: "19px",fontFamily:'Luminari'}} key={i}>{item}</p>;
    });
  return (
    
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
          <div className={classes.typo}>
              <h2 style={{fontFamily: "Courier New",fontSize:'36px'}}>{title}</h2>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
            </div>
            {/*<h4 style={{fontSize: "17.5px"}} >{text}</h4> */}
            {newText}
            <br/>
            
          </GridItem>
        </GridContainer>
     
  );
}
export default withTranslation()( SectionCompletedExamples);