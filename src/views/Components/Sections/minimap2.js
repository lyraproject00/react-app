import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Minimap2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
    
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11,
        }
    }
  

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '1000px', width: '1000px' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyA8k2utFV196NDfL2ioPEkta0DGgMF9i0I' }}
          defaultCenter={{center:{lat: 59.95,lng: 30.33}}}
          defaultZoom={{zoom: 1}}
        
        >
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="My Marker"
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default Minimap2;