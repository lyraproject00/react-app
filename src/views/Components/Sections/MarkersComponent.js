import React from "react";

import { withTranslation } from 'react-i18next';

import brflag from 'assets/img/brfilter.png'
import enflag from 'assets/img/enfilter.png'
import enbrflag from 'assets/img/enbrfiltered2.png'

import {Marker} from 'google-maps-react';

function MarkersComponent(props) {
    const { t } = props;
    let item = props.item
    if (item.localizations[0] == undefined) {
      return false; // skip
    }
    
    
    let res = `titles.${item.id}`
    let res2 = `texts.${item.id}`
    let title
    let flag
    let description
    switch (item.language) {
      case 'por':
          title = item.BRtitle
          description = item.BRdescription
          flag = brflag
          //2console.log('portuguese');
          break;
      case 'eng':
          title = item.ENtitle
          description = item.ENdescription
          flag = enflag
          //console.log('english');
          break;
      case 'eng/por':
          title = t(res)
          description = t(res2)
          flag = enbrflag
          //console.log('both');
          break;
  }
    return (<Marker
    id={item.id}
    title={title}
    description={description}
    flag={flag}
    onClick={props.onmarkerclick}
    position={{
        lat: item.localizations[0].lat,
        lng: item.localizations[0].lng
      }}
    icon={{
        url: require("assets/img/aliengreen3.png"),
        anchor: props.mapspoint,
        scaledSize: props.mapssize
      }}
      />)       
    

   

}

export default withTranslation()(MarkersComponent);  