/*eslint-disable*/
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
// react components for routing our app without refresh
import { Link } from "react-router-dom";
import i18n from 'i18next';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import { withTranslation } from 'react-i18next';
// @material-ui/icons
import { Apps, PagesSharp ,FlagSharp, Tablet, Map} from "@material-ui/icons";

// core components
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";
import { useCookies } from "react-cookie";
import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

  function HeaderLinks(props) {
  const { t } = props;
  const classes = useStyles();
  const [cookies, setCookie] = useCookies(["lng"]);
  function handleCookieEN() {
    setCookie("lng", "en", {   sameSite:"lax" ,  path: "/"    });
    i18n.changeLanguage('en')
  }
  function handleCookieBR() {
    setCookie("lng", "pt_br", {   sameSite:"lax" ,  path: "/"    }); 
    i18n.changeLanguage('pt_br') 
  }
 
  return (
    
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          hoverColor="black"
          buttonText="Lng"
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          buttonIcon={FlagSharp}
          dropdownList={[
            <div
              onClick={handleCookieEN} 
              className={classes.dropdownLink}
            >
              EN
            </div>,
            <div
              onClick={handleCookieBR} 
              className={classes.dropdownLink}
            >
              PT-BR
            </div>
          ]}
        />
      </ListItem>
      
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          hoverColor="black"
          buttonText={t("header4")}
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          buttonIcon={Map}
          dropdownList={[
            <Link to="/about" className={classes.dropdownLink}>
              {t("header1")}
            </Link>,
            <Link to="/lyradb" className={classes.dropdownLink}>
              {t("header2")}
            </Link>,
            <Link to="/gmap2" className={classes.dropdownLink}>
            {t("header3")}
          </Link>,
          ]}
        />
      </ListItem>
      
      
    </List>
  );
}
export default withTranslation()(HeaderLinks);  