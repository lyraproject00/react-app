import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import axios from "axios";

console.log(document.cookie)
let lng
switch (document.cookie) {
  case 'lng=en':
      lng = 'en'
      break;
  case 'lng=pt_br':
      lng = 'pt_br'
      break;
}
console.log(lng)
let res
//"http://localhost:8000/api/registry/"
  //"https://lyraproject0.herokuapp.com/api/registry/"
axios.get("https://lyraproject0.herokuapp.com/api/registry/").then((response) => res = response.data).finally(() => {
  console.log('b');
let ENtitles = res.reduce((map, obj) => (map[obj.id] = obj.ENtitle, map), {});
let ENtexts = res.reduce((map, obj) => (map[obj.id] = obj.ENtext, map), {});
let BRtitles = res.reduce((map, obj) => (map[obj.id] = obj.BRtitle, map), {});
let BRtexts = res.reduce((map, obj) => (map[obj.id] = obj.BRtext, map), {});
//const ENRegistry = res.map(item => ({ title: item.ENtitle, text: item.ENtext }) );
//const BRRegistry = res.map(item => ({ title: item.BRtitle, text: item.BRtext }) );
//console.log(ENRegistry)
//console.log(BRRegistry)

const resources= {
  en: {
    translation: {
     titles: ENtitles,
     texts:  ENtexts
    }
  },
  pt_br: {
    translation: {
      titles: BRtitles ,
      texts: BRtexts   
    }
  }
}

const options = {
  order: ['querystring', 'navigator'],
  lookupQuerystring: 'lng'
}

//let index = res.id - 1
//let res1 = resources.en.translation.Registry[index].title
console.log(resources)
//console.log(res1)
i18n
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    resources,
    //detection: options,
    lng: lng ? lng : 'en',
    debug: true,
    fallbackLng: 'en',
    supportedLngs: ['en', 'pt_br'],
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },

    
  });
});
export default i18n;
