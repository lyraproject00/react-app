import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import './styles2.css'
import { CookiesProvider } from "react-cookie";
import "assets/scss/material-kit-react.scss?v=1.9.0";

// pages for this product
import Registry from "views/Pages/registry.js";
import Registry2 from "views/Pages/registry2.js";
import Registry3 from "views/Pages/registry3.js";
import Registry4 from "views/Pages/registry4.js";
import LandingPage from "views/Pages/LandingPage.js";
import LandingPage2 from "views/Pages/LandingPage2.js";
import LandingPage3 from "views/Pages/LandingPage3.js";
import LandingPage4 from "views/Pages/LandingPage4.js";
import LandingPage5 from "views/Pages/LandingPage5.js";
import map from "views/Pages/map.js"
import gmap from "views/Pages/gmap.js"
import Gmap2 from "views/Pages/gmap2.js"
import simplemap from "views/Pages/simplemap.js"

var hist = createBrowserHistory();

ReactDOM.render(
  <React.Suspense fallback="Loading...">
  <CookiesProvider>
  <Router history={hist}>
    <Switch>
      <Route exact path="/">
        <Redirect to="/lyradb5" /> </Route>
      <Route path="/lyradb" component={LandingPage5} />
      <Route path="/lyradb2" component={LandingPage} />
      <Route path="/lyradb3" component={LandingPage2} />
      <Route path="/lyradb4" component={LandingPage3} />
      <Route path="/lyradb5" component={LandingPage4} />
      <Route path="/map" component={map} />
      <Route path="/gmap" component={gmap} />
      <Route path="/gmap2/:par1?/:par2?" component={Gmap2} />
      <Route path="/smap" component={simplemap} />
      <Route path="/registry2/:id" component={Registry} />
      <Route path="/registry3/:id" component={Registry2} />
      <Route path="/registry4/:id" component={Registry4} />
      <Route path="/registry/:id" component={Registry3} />
    </Switch>
  </Router>
  </CookiesProvider>
  </React.Suspense>,
  document.getElementById("root")
);
